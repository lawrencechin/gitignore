#!/bin/bash

# This script utilises the cloned github ignore repository 
# It reads individual files into a single .gitignore that is place into the supplied path
# .gitignore file is placed in the cwd 

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

touch "$1/.gitignore"
cat "$DIR/gitignore"/Global/Archives.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/Dropbox.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/Linux.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/macOS.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/SublimeText.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/SVN.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/Vim.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/Windows.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Global/Xcode.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/clojure.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/node.gitignore >> "$1/.gitignore"
cat "$DIR/gitignore"/Sass.gitignore >> "$1/.gitignore"
